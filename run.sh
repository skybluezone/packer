#!/bin/bash
export PACKER_LOG=1

temp_role=$(aws sts assume-role \
    --role-arn "$1" \
    --role-session-name "$2")

export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq -r .Credentials.AccessKeyId)
export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq -r .Credentials.SecretAccessKey)
export AWS_SESSION_TOKEN=$(echo $temp_role | jq -r .Credentials.SessionToken)

packer build $3

